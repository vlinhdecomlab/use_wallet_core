import 'dart:convert';
import 'dart:math';
import 'package:http/http.dart' as http;

class BinanceBlockchainProvider {
  static const _nodeUrl = "https://bsc-dataseed.binance.org/";

  Future<double> getBalance(String address) async {
    final url = Uri.parse(_nodeUrl);
    final data = {
      'method': _Method.getBalance,
      'params': [address, _Method.latestBlock],
      'id': 1,
      'jsonrpc': '2.0'
    };
    final response = await http.post(url, body: jsonEncode(data));
    if (response.statusCode == 200) {
      final result = jsonDecode(response.body)["result"] as String;
      return BigInt.parse(result.substring(2)) / BigInt.from(pow(10, 18));
    } else {
      return 0.0;
    }
  }
}

class _Method {
  static const getBalance = "eth_getBalance";
  static const latestBlock = "latest";
}
