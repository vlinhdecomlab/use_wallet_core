import 'package:basesource/binance_blockchain_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wallet_core/wallet_core.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo Wallet Core',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Wallet Core'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _isLoading = true;
  String _mnemonic = "Loading...";
  String _address = "Loading...";
  double _value = 0.0;

  final WalletCore _walletCore = WalletCore();
  final BinanceBlockchainProvider _binanceBlockchainProvider =
      BinanceBlockchainProvider();

  void _genAddressForBinanceSmartChain() async {
    setState(() {
      _isLoading = true;
      _mnemonic = "Loading...";
      _address = "Loading...";
    });
    _mnemonic = await _walletCore.create();
    _address = await _walletCore.genAddressFromMnemonic();
    _value = await _binanceBlockchainProvider.getBalance(_address);
    _isLoading = false;
    setState(() {});
  }

  @override
  void initState() {
    _genAddressForBinanceSmartChain();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: _isLoading
            ? const CupertinoActivityIndicator()
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Data',
                    style: Theme.of(context).textTheme.headline4,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Text(
                      _mnemonic,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(bottom: 16.0),
                    child: Text(
                      _address,
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                  Text(
                    _value.toString(),
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ],
              ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: !_isLoading ? _genAddressForBinanceSmartChain : () {},
        tooltip: 'Gen',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
