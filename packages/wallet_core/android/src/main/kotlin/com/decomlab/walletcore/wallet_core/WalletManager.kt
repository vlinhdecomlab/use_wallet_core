package com.decomlab.walletcore.wallet_core

import com.google.protobuf.Any
import wallet.core.jni.CoinType
import wallet.core.jni.HDWallet

class WalletManager {
    init {
        System.loadLibrary("TrustWalletCore")
    }
    private  lateinit var wallet: HDWallet
    var callData = listOf<Any>()
    var resultData: kotlin.Any = ""

    fun createHDWallet(){
        val strength = callData[0] as Int
        val passphrase = callData[1] as String
        wallet = HDWallet(strength, passphrase)
        resultData = wallet.mnemonic()
    }

    fun genAddressFromMnemonic() {
        val coinId = callData[0] as Int
        val index = callData[1] as Int
        val coin: CoinType = CoinType.createFromValue(coinId)
        var derivationPath = coin.derivationPath()
        if (derivationPath.dropLast(1) != "'"){
                derivationPath.plus(index.toString())
        }
        else {
                derivationPath.dropLast(1)
                derivationPath.plus("$index'")
        }
        val privateKey = wallet.getKey(coin, derivationPath)
        resultData = coin.deriveAddress(privateKey)
    }
}