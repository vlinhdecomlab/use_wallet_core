import Flutter
import UIKit

public enum CallMethod: String {
    case create = "create"
    case genAddressFromMnemonic = "genAddressFromMnemonic"
}

public class SwiftWalletCorePlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "wallet_core", binaryMessenger: registrar.messenger())
        let instance = SwiftWalletCorePlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    private let walletManager = WalletManager()
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        walletManager.callData =  call.arguments as! [Any]
        if let safeMethod = CallMethod.init(rawValue: call.method) {
            switch safeMethod {
            case .create:
                walletManager.createHDWallet()
            case .genAddressFromMnemonic:
                walletManager.genAddressFromMnemonic()
            }
        }
        result(walletManager.resultData)
    }
}

