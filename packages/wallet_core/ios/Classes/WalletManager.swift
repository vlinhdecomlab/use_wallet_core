//
//  WalletManager.swift
//  wallet_core
//
//  Created by niBVL on 31/10/2021.
//

import Foundation
import WalletCore

class WalletManager {
    private var wallet: HDWallet?
    var callData = [Any]()
    var resultData: Any = ""
    
    func createHDWallet() {
        let strength = callData[0] as! Int32
        let passphrase = callData[1] as! String
        wallet = HDWallet(strength: strength, passphrase: passphrase)
        resultData = wallet!.mnemonic
    }
    
    func genAddressFromMnemonic() {
        let coinId = callData[0] as! Int32
        let index = callData[1] as! Int32
        if let safeCoin = CoinType.init(rawValue: UInt32(coinId)) {
            var derivationPath = safeCoin.derivationPath()
            if derivationPath.popLast() != "'" {
                derivationPath.append(String(index))
            }
            else {
                _ = derivationPath.popLast()
                derivationPath.append(String(index)+"'")
            }
            let privateKey = wallet?.getKey(coin: safeCoin , derivationPath: safeCoin.derivationPath())
            if let safePrivateKey = privateKey {
                resultData = safeCoin.deriveAddress(privateKey: safePrivateKey)
            }
        }
    }
}
