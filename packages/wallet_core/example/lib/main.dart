import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:wallet_core/wallet_core.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<String> _platformVersion = ['Unknown', 'Unknown'];

  @override
  void initState() {
    super.initState();
    createWallet();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> createWallet() async {
    List<String> data = [];
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      final mnemonic = await WalletCore().create();
      data.add(mnemonic);
      final address = await WalletCore().genAddressFromMnemonic(index: 1);
      data.add(address);
    } on PlatformException {
      data.add("Failure");
      data.add("Failure");
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = data;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child:
              Text('Data:\n\t${_platformVersion[0]}\n\t${_platformVersion[1]}'),
        ),
      ),
    );
  }
}
