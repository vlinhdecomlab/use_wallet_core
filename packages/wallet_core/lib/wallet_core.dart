import 'dart:async';

import 'package:flutter/services.dart';

class _Method {
  static const create = "create";
  static const genAddressFromMnemonic = "genAddressFromMnemonic";
}

class WalletCore {
  factory WalletCore() => WalletCore._internal();

  WalletCore._internal();

  final MethodChannel _channel = const MethodChannel('wallet_core');

  Future<String> create({
    strength = 128,
    passphrase = "",
  }) async {
    final String mnemonic =
        await _channel.invokeMethod(_Method.create, [strength, passphrase]);

    return mnemonic;
  }

  Future<String> genAddressFromMnemonic({
    coinId = 20000714,
    index = 0,
  }) async {
    final String address = await _channel
        .invokeMethod(_Method.genAddressFromMnemonic, [coinId, index]);
    return address;
  }
}
